# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).


## 0.1.0 - 2020-08-20
### Added

[Unreleased]: https://gitlab.com/lxsameer/halley/compare/0.1.1...HEAD
[0.1.0]: https://gitlab.com/lxsameer/halley/compare/0.1.0...0.1.1
